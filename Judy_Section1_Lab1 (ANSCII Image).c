// Basketball Shot
// Author: Alex Judy
// Date: 1/7/2014
// Section: 1


#include <stdio.h>

main()
{


printf("                                                                     ____ \n");
printf("              OOOOOOOOOOO                                           |    |\n");
printf("             OOOOOOOOOOOOO                                          |    |\n");
printf("            OOOOOOOOOOOOOOO                                         |    |\n");
printf("           OOOOOOOOOOOOOOOOO                                        |    |\n");
printf("          OOOOOOOOOOOOOOOOOOO                                       |    |\n");
printf("           OOOOOOOOOOOOOOOOO                                        |    |\n");
printf("            OOOOOOOOOOOOOOO                                         |    |\n");
printf("             OOOOOOOOOOOOO                                          |    |\n");
printf("              OOOOOOOOOOO                                           |    |\n");
printf("       ///////                                                      |    |\n");
printf("     ///////                                                        |    |\n");
printf("   ///////                         _________________________________|    |\n");
printf(" ///////                          |_________________________________|    |\n");
printf("                                    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX |    |\n");
printf("                                     XXXXXXXXXXXXXXXXXXXXXXXXXXXXX  |    |\n");
printf("                                      XXXXXXXXXXXXXXXXXXXXXXXXXXX   |    |\n");
printf("                                       XXXXXXXXXXXXXXXXXXXXXXXXX    |____|\n");
printf("                                        XXXXXXXXXXXXXXXXXXXXXXX       |  |\n");
printf("                                         XXXXXXXXXXXXXXXXXXXXX        |  |\n");
printf("                                          XXXXXXXXXXXXXXXXXXX         |  |\n");
printf("                                           XXXXXXXXXXXXXXXXX          |  |\n");
printf("                                            XXXXXXXXXXXXXXX           |  |\n");
printf("                                                                      |  |\n");
printf("                                                                      |  |\n");
printf("                                                                      |  |\n");
printf("                                                                      |  |\n");
printf("                                                                      |  |\n");
printf("                                                                      |  |\n");
printf("                                                                      |  |\n");
printf("                                                                      |  |\n");
printf("                                                                      |  |\n");
printf("                                                                      |  |\n");
printf("                                                                      |  |\n");
printf("                                                                      |  |\n");
printf("                                                                      |  |\n");
printf("                                                                      |  |\n");
printf("                                                                      |  |\n");
printf("                                                                      |  |\n");
printf("                                                                      |  |\n");
printf("                                                                      |  |\n");
printf("______________________________________________________________________|__|\n");

system("PAUSE");
} //End program
